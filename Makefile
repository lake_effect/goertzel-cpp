ROOT_DIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
CXX := g++
CXXFLAGS := -arch x86_64 -std=c++11 -stdlib=libc++
WFLAGS := -Wall

# Compile object file
goertzel.o: goertzel.cpp goertzel.h
	$(CXX) $(WFLAGS) $(CXXFLAGS) -fPIC -c $< -o $@
# Make the goertzel shared object
libgoertzel.so: goertzel.o
	$(CXX) -shared $< -o $@
# Compile the test object file
test.o: test/main.cpp
	$(CXX) $(CXXFLAGS) -DTEST -c $< -o $@
# Link the test binary
tester: test.o libgoertzel.so
	$(CXX) -L$(ROOT_DIR) -lgoertzel $< -o $@
# Run the binary
check: tester
	$(ROOT_DIR)/tester
# Clean the output
clean:
	rm *.so *.o tester
# Declare phony targets
.PHONY: check clean
