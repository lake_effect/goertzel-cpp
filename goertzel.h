#ifndef GOERTZEL_H
#define GOERTZEL_H

#include <cmath>
#include <complex>
#include <iostream>
#include <tuple>
#include <vector>

using std::vector;
using std::complex;
using std::tuple;

namespace goertzel {
  vector< complex<double> > filter_naive(const vector<double> &input,
                                  double linear_freq);
  complex<double> dft(const vector<double> &input, double linear_freq);
  double dft_power(const vector<double> &input, double linear_freq);
}

#endif
