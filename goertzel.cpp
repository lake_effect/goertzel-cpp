#include "goertzel.h"

namespace goertzel {
  vector<double> intermediate_filter(const vector<double> &input,
                                     double angular_freq) {
    double freq_term = 2.0 * cos(angular_freq);

    vector<double> intermediate_vec;
    intermediate_vec.resize(input.size());

    intermediate_vec.push_back(input[0]);
    intermediate_vec.push_back(input[1] + freq_term * input[0]);

    // Iterating over input indices, skipping the first two from above
    for(vector<double>::size_type ind = 2; ind != input.size(); ind++) {
      double filter_term = input[ind] + freq_term * intermediate_vec[ind - 1] -
        intermediate_vec[ind - 2];
      intermediate_vec.push_back(filter_term);
    }

    return intermediate_vec;
  }

  vector< complex<double> > filter_naive(const vector<double> &input,
                                         double linear_freq) {
    double angular_freq = linear_freq / 2.0 / M_PI;
    vector<double> intermediate_vec =
      intermediate_filter(input, angular_freq);

    vector< complex<double> > output;
    output.reserve(input.size());

    output.push_back(intermediate_vec[0]);

    complex<double> j = 0 + 1i;
    complex<double> exponential_term = pow(M_E, (-j * angular_freq));

    // Iterating over input indices, skipping first one from above
    for(vector<double>::size_type ind = 1; ind != input.size(); ind++) {
      output.push_back(input[ind] - exponential_term * intermediate_vec[ind - 1]);
    }

    return output;
  }

  tuple<int, double> bin_freq(const double &linear_freq,
                                   int signal_length) {
    double sig_len = signal_length;
    double angular_freq = linear_freq / 2.0 / M_PI;

    double approx_bin_num = angular_freq * sig_len / 2.0 / M_PI;
    int bin_num = approx_bin_num;
    double bin_freq = bin_num / sig_len * 2.0 * M_PI;

    if (approx_bin_num != (double) bin_num) {
      std::cout << "Warning: aliasing frequency to "
                << bin_freq * 2.0 * M_PI
                << " to fit in bin...\n";
    }

    return std::make_tuple(bin_num, bin_freq);
  }

  complex<double> dft(const vector<double> &input, double linear_freq) {
    double freq = std::get<1>(bin_freq(linear_freq, input.size()));

    vector<double> intermediate_vec =
      intermediate_filter(input, freq);
    intermediate_vec.pop_back();

    complex<double> j = 0 + 1i;
    complex<double> exponential_term = pow(M_E, (-j * freq));

    double state_1 = intermediate_vec.back();
    double state_2 = intermediate_vec.at(intermediate_vec.size() - 1);
  
    return exponential_term * state_1 - state_2;
  }

  double dft_power(const vector<double> &input, double linear_freq) {
    double freq = std::get<1>(bin_freq(linear_freq, input.size()));

    vector<double> intermediate_vec =
      intermediate_filter(input, freq);
    intermediate_vec.pop_back();

    double state_1 = intermediate_vec.back();
    double state_2 = intermediate_vec.at(intermediate_vec.size() - 1);

    return pow(state_1, 2) + pow(state_2, 2) -
      2.0 * cos(freq) * state_1 * state_2;
  }
}
