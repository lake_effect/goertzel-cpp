#include "../lib/bugeye/BugEye3.h"
#include "../goertzel.h"

using std::vector;

static const double TAU = 2.0 * M_PI;
static const double EPS = 0.00001;


vector<double> first_difference(const vector<double> &input) {
  // First difference produces output of size one less than input
  vector<double> output_vec;
  output_vec.resize(input.size() - 1);

  // Iterate over input indices and stop at .size() - 1
  for(vector<double>::size_type ind = 0; ind != input.size() - 1; ind++) {
    output_vec.push_back(input[ind] - input[ind + 1]);
  }

  return output_vec;
}

// Borrowed from https://gist.github.com/jmbr/2375233
vector<double> linspace(double a, double b, size_t N) {
  double h = (b - a) / static_cast<double>(N-1);
  vector<double> xs(N);
  typename vector<double>::iterator x;
  double val;
  for (x = xs.begin(), val = a; x != xs.end(); ++x, val += h)
    *x = val;
  return xs;
}


static auto dft_linearity = bugeye::test("DFT is linear") = [] {
  const double freq = 440.0;
  const double interval_length = 1.0;
  const double delta = interval_length / freq / 2.0;

  vector<double> time_1s = linspace(0.0, interval_length, (size_t) (freq / 2.0));

  transform(time_1s.begin(), time_1s.end(), back_inserter(time_1s),
            [=](double ind) {
              return delta  * ind;
            });

  vector<double> sine_440;
  vector<double> sine_100;
  vector<double> summed_signal;
  sine_440.resize(time_1s.size());
  sine_100.resize(time_1s.size());
  summed_signal.resize(time_1s.size());

  transform(time_1s.begin(), time_1s.end(), sine_440.begin(),
            [=](double time_sample) {
              return sin(freq * time_sample);
            });
  transform(time_1s.begin(), time_1s.end(), sine_100.begin(),
            [=](double time_sample) {
              return sin(100.0 * time_sample);
            });

  for(vector<double>::size_type ind = 0; ind != sine_440.size(); ind++) {
    summed_signal[ind] = sine_440[ind] + sine_100[ind];
  }

  complex<double> dft_440 = goertzel::dft(sine_440, freq);
  complex<double> dft_100 = goertzel::dft(sine_100, freq);
  complex<double> dft_summed_signal = goertzel::dft(summed_signal, freq);
  complex<double> dft_sum = dft_440 + dft_100;
  
  complex<double> complex_diff = dft_summed_signal - dft_sum;
  
  OK(std::abs(complex_diff.real()) < EPS);
  OK(std::abs(complex_diff.imag()) < EPS);
};

static auto dft_power_linearity =
  bugeye::test("DFT power is linear") = [] {

  const double freq = 440.0;
  const double interval_length = 1.0;
  const double delta = interval_length / freq / 2.0;

  vector<double> time_1s = linspace(0.0, interval_length, (size_t) (freq / 2.0));

  transform(time_1s.begin(), time_1s.end(), back_inserter(time_1s),
            [=](double ind) {
              return delta  * ind;
            });

  vector<double> sine_440;
  vector<double> sine_100;
  vector<double> summed_signal;
  sine_440.resize(time_1s.size());
  sine_100.resize(time_1s.size());
  summed_signal.resize(time_1s.size());

  transform(time_1s.begin(), time_1s.end(), sine_440.begin(),
            [=](double time_sample) {
              return sin(freq * time_sample);
            });
  transform(time_1s.begin(), time_1s.end(), sine_100.begin(),
            [=](double time_sample) {
              return sin(100.0 * time_sample);
            });

  for(vector<double>::size_type ind = 0; ind != sine_440.size(); ind++) {
    summed_signal[ind] = sine_440[ind] + sine_100[ind];
  }

  double power_440 = goertzel::dft_power(sine_440, freq);
  double power_100 = goertzel::dft_power(sine_100, freq);
  double power_summed_signal = goertzel::dft_power(summed_signal, freq);
  double power_sum = pow(std::sqrt(power_440) + std::sqrt(power_100), 2);

  OK(std::abs(power_sum - power_summed_signal) < EPS);
};

static auto dft_const =
  bugeye::test("DFT of impulse is constant w.r.t. frequency") = [] {

  double max_freq = 500.0;
  double interval_length = 0.5;

  double delta = interval_length / max_freq / 2.0;

  vector<double> time_500ms = linspace(0.0, interval_length, (size_t) (max_freq / 2.0));

  transform(time_500ms.begin(), time_500ms.end(), back_inserter(time_500ms),
            [=](double sample) {
              return delta  * sample;
            });

  vector<double> impulse_500ms(time_500ms.size(), 0);
  impulse_500ms[0] = 1.0;

  vector<double> dft_bins = {1.0, 10.0, 100.0, 250.0, 300.0, 500.0};

  vector<complex<double> > impulse_dfts(dft_bins.size(), 0.0);

  transform(dft_bins.begin(), dft_bins.end(), impulse_dfts.begin(),
            [=](double dft_freq) {
              return goertzel::dft(impulse_500ms, dft_freq);
            });

  for(vector<double>::size_type ind = 0; ind <= impulse_dfts.size() - 2; ind+=2) {
    double dft_diff = std::abs(impulse_dfts[ind]) - std::abs(impulse_dfts[ind + 1]);
    OK(dft_diff < EPS);
  }
};

static auto dft_const_power =
  bugeye::test("Power of impulse is constant w.r.t. frequency") = [] {

  double max_freq = 500.0;
  double interval_length = 0.5;

  double delta = interval_length / max_freq / 2.0;

  vector<double> time_500ms = linspace(0.0,
                                       interval_length,
                                       (size_t) (max_freq / 2.0));

  transform(time_500ms.begin(), time_500ms.end(), back_inserter(time_500ms),
            [=](double sample) {
              return delta  * sample;
            });

  vector<double> impulse_500ms(time_500ms.size(), 0);
  impulse_500ms[0] = 1.0;

  vector<double> dft_bins = {1.0, 10.0, 100.0, 250.0, 300.0, 500.0};

  vector<double> impulse_dfts(dft_bins.size(), 0.0);

  transform(dft_bins.begin(), dft_bins.end(), impulse_dfts.begin(),
            [=](double dft_freq) {
              return goertzel::dft_power(impulse_500ms, dft_freq);
            });

  for(vector<double>::size_type ind = 0; ind <= impulse_dfts.size() - 2; ind+=2) {
    OK(impulse_dfts[ind] - impulse_dfts[ind + 1] < EPS);
  }
};

static auto dft_const_phase_difference =
  bugeye::test("Phase of DFTs of time-offset impulses is linear") = [] {
  double max_freq = 500.0;
  double interval_length = 0.5;

  double delta = interval_length / max_freq / 2.0;

  vector<double> time_500ms = linspace(0.0,
                                       interval_length,
                                       (size_t) (max_freq / 2.0));

  transform(time_500ms.begin(), time_500ms.end(), back_inserter(time_500ms),
            [=](double sample) {
              return delta  * sample;
            });

  vector<double> impulse_500ms(time_500ms.size(), 0);
  impulse_500ms[0] = 1.0;

  vector<double> dft_bins = {1.0, 10.0, 100.0, 250.0, 300.0, 500.0};

  vector<double> dft_phases(dft_bins.size(), 0.0);

  transform(dft_bins.begin(), dft_bins.end(), dft_phases.begin(),
            [=](double dft_freq) {
              return arg(goertzel::dft(impulse_500ms, dft_freq));
            });

  vector<double> dft_phase_diffs = first_difference(dft_phases);
  transform(dft_phase_diffs.begin(), dft_phase_diffs.end(), back_inserter(dft_phase_diffs),
            [=](double diff) {
              return fmod(( diff + TAU ), TAU);
            });

  for(vector<double>::size_type ind = 0; ind <= dft_phase_diffs.size() - 2; ind+=2) {
    OK(dft_phase_diffs[ind] - dft_phase_diffs[ind + 1] < EPS);
  }
};
