# goertzel-cpp

C++ implementation of the Goertzel filter.

The Goertzel filter is faster than the FFT for a small range of frequencies of
interest.

Compare to the [Rust implementation](https://gitlab.com/bright-star/goertzel).


## interface

- `vector< complex<double> > filter_naive(const vector<double> &input, double linear_freq)`

Just applies the filter equations and gives you the complete output.

- `complex<double> dft(const vector<double> &input, double linear_freq)`

Computes the associated (closest) DFT term to a given linear frequency.

- `double dft_power(const vector<double> &input, double linear_freq)`

Computes the power of the signal at a given linear frequency.
